# Chapter 8. Geospatial on the web

During this course you have learned how to setup and utilize a python pipeline to collect- , clean data and perform a geospatial analysis. As we have seen in chapter 4, we can visualize data using python by utilizing charts.

In this chapter we are going to look at the python module ```folium```. Folium can be used to generate geodata visualizations  that van be used on the web (web maps).



## Rationale

Data that we have created for TAA2 & TAA4 can be useful in a desktop GIS environment (like ArcGIS & QGIS). But it is also a good idea to broaden your horizon and look how we can utilize the data in online maps.

## Outcomes

- basic proficiency with the folium module

## Resources

### Reference

[Folium documentation](https://python-visualization.github.io/folium/).

## Lecture notes
In the sections below we will install folium and look at 4 examples of folium maps.

### Installing folium
We have two options to install folium: pip and conda
To install folium via pip, use the following command:
```
pip install folium
```

Folium is not part of the basic conda repository, but a definition of folium is available via the community-led repository conda-forge. To install packages form conda-forge, we must specify it as the channel to use. For folium it is done as follows:
```
conda install folium -c conda-forge
```

To check if you have folium successfully installed (and if you are missing any other dependencies for this chapter), add the following imports to your python script:

```python
import folium
from folium import Choropleth, Marker
from folium.plugins import MarkerCluster

import geopandas as gpd
```

### Your first map
Creating basic maps with folium is quite simple. 
When we look at the QuickStart in the folium documentation we can see that it takes two lines:

```python
# Create a map
m_1 = folium.Map(location=[52,5], tiles='openstreetmap', zoom_start=7)

# Save the map
m_1.save('./results/m1.html')
```
These two lines will create a html file which will display a basic interactive openstreetmap web application (open the html-file from the results folder in a web browser). 
![](./images/m1.png)
This first result is nice, but not very interesting or useful. Adding our own data will fix that.

### Loading data
In the data folder of this chapter, you will find two geospatial datasets:
- amsterdam.shp: simplified dataset of the neighborhoods of Amsterdam.
- polling_stations_amsterdam.geojson: simplified dataset of the polling stations of Amsterdam.

To load our data in python we will use geopandas. Add the following lines to your script and see what is in the datasets:

```python
# load polling stations
polling_stations = gpd.read_file('./data/polling_stations_amsterdam.geojson')
print(polling_stations.head())

# load neighborhoods
amsterdam = gpd.read_file("./data/amsterdam.shp")
print(amsterdam.head())
```

### Adding points
Time to add data to a folium map. We will start by creating markers for every polling station and adding them to a map.

```python
# Create a map
m_2 = folium.Map(location=[52.36,4.9], tiles='cartodbpositron', zoom_start=11)

# Add points to the map
for idx, row in polling_stations.iterrows():
    Marker([row['geometry'].y, row['geometry'].x]).add_to(m_2)

# Save the map
m_2.save('./results/m2.html')
```
![](./images/m2.png)

Each polling station is now visualized on the map! If we zoom in, we can see the exact location of each polling station. On a large-scale map this is very useful. On a small-scale map, the dots overlap. This makes it difficult to see where most of the polling stations are.

### Clustering points
A solution for the overlapping markers is to cluster them. This is done by creating a MarkerCluster object and adding all the point markers to it:

```python
# Create a map
m_3 = folium.Map(location=[52.36,4.9], tiles='cartodbpositron', zoom_start=11)

# Add points to the map
mc = MarkerCluster()
for idx, row in polling_stations.iterrows():
    mc.add_child(Marker([row['geometry'].y, row['geometry'].x]))
m_3.add_child(mc)

# Save the map
m_3.save('./results/m3.html')
```
![](./images/m3.png)

It is now much easier to see where the most polling stations are, while still seeing the exact location after zooming in to the map.

### Choropleth maps
In the examples thus far, we have only worked with point data. Let's change this by creating a choropleth map!

As with the point maps, we first define a base map. After which we can define the choropleth. Because the choropleth object works with the entire dataset, we can directly specify all necessary information in its construction. When the choropleth is successfully created it is added directly to the base map. The last line of code is, again, saving the map to a html file.

The following code creates a choropleth map based on the population density of the neighborhoods of Amsterdam:

```python
# Create a base map
m_6 = folium.Map(location=[52.36,4.9], tiles='cartodbpositron', zoom_start=12)

# Add a choropleth map to the base map
Choropleth(
    geo_data=amsterdam,
    data=amsterdam,
    key_on="feature.properties.WK_NAAM",
    columns=['WK_NAAM','BEV_DICHT'],
    fill_color='YlGnBu',
    legend_name='Population density per km2'
).add_to(m_6)

# Save the map
m_6.save('./results/m4.html')
```
![](./images/m4.png)

### When generating maps is not enough
The above examples use some of the features of folium to generate maps. But folium is not a one-stop solution for creating geospatial web applications.

Folium uses the JavaScript library [Leaflet](https://leafletjs.com/). While folium can do a lot, the Leaflet library has more options that can be used. However, to use these options, you must interact directly with the library using JavaScript.

When you start using JavaScript, other (more extensive) libraries also become available. For web mapping, a commonly used library is [openlayers](https://openlayers.org/). But by doing this, you are essentially building a web application from scratch. This means that you will use:
- HTML: to create the basic layout of the web page.
- CSS: to format the web page.
- JavaScript: to add interactivity to the web page and to load data.

For (non-spatial) web pages, there are a multitude of libraries and frameworks that you could use to create your "perfect" web application. For example, take a look at the following [blog post](https://blog.templatetoaster.com/handpicked-frameworks-libraries-web-design/).

## Exercises
Try to combine the two datasets form the examples above in one web map. Search in the folium documentation for the possibility to add pop-ups with information.

Think about how you would visualize your results from TAA4 in a geospatial web application.