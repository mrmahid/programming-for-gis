# Chapter 2. Python programming 1 - the basics

In this module you will get acquainted with the programming language Python.

## Rationale

Python is a friendly programming language that is very popular amongst researchers, data scientists, geographical information experts and web developers. It has a rich ecosystem which means that many people are writing add-ons and extensions to it (called modules) thereby making it the right tool for a myriad of (geo-spatial) tasks. E.g. you can leverage Google's machine learning toolkit TensorFlow in Python.

## Outcomes

- basic proficiency in Python
  - string (pieces of text) manipulation
  - flow control (decision making)
  - built-in functions (bundled and named pieces of functionality)
  - list and dictionaries (key-value stores)
  - iteration (going through the items in a list)
- experience in reading and debugging code

## Resources

### Course

Methods to learn the basics of Python.

| Course type | Course                                                                  | Relevant chapters                         | Remarks                                                                                                                             |
| ----------- | ----------------------------------------------------------------------- | ---------------------------------------------: | ------------------------------------------------------------------------------------------------------------ |
| Website     | [Learn Python](https://www.learnpython.org/)                            |learn the basics: "Hello world" to "Loops" | Best suited for students who are just starting with programming and prefer a step-by-step interactive course |
| Book        | [The Coder's Apprentice](http://www.spronck.net/pythonbook/index.xhtml) | 1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 13       | Best suited for students with no prior knowledge who prefer text-based study (available in english and dutch).                                |

The Learn Python website provides a brief but thorough introduction to the Python programming language. For a more in-depth explanation and more exercises, you can follow the chapters of The Coder's Apprentice. In these resources you use an online python interpreter and a simple local interpreter. During this course you will expand your own local development environment with extra packages (functionalities). As a result, the lecture notes are based on a Python anaconda installation (environment and package manager). How to install and use anaconda is described in the lecture notes below.

Anaconda is not required. If you are familiar with any other Python environment/interpreter, you are free to use it!

### Reference

- [The official Python tutorial](https://docs.python.org/3/tutorial/index.html) on python.org

### Lecture notes

In the remainder of this chapter we will touch upon an activity you will spend a lot of time doing: debugging. 

### Installing Python (Anaconda)

You'll need to install Python and a number of external modules on your machine to run the example scripts and perform the exercises. To make it easier we will use a Python distribution that already comes with a large number of modules that are useful for scientific computing, called Anaconda. Anaconda also comes with a module manager called Conda, which we will use to install a few modules.

In this course we will use Python 3, but after this course you may encounter Python version 2.7. There are some differences between them, but based on the skills you will learn here, they won't be very noticeable. For a long time Python 2 stayed the most used version in the GIS world. At the moment we are seeing more and more applications make the shift to Python 3. ArcGIS Pro and QGIS 3, for example, both use Python 3, while ArcGIS Desktop (ArcMap) still uses Python 2. However keep in mind that Python 3 is the future, and the Python 2 is no longer supported by the [Python software foundation](https://www.python.org/psf/) since January 1st, 2020.

Download and install Anaconda -> [Download Anaconda](https://www.anaconda.com/download/) and double click it. You can accept the default choices.

In this course we will use the Spyder code editor that also comes with Anaconda. You can start the Spyder editor using the Anaconda Navigator application. Spyder consists of three main components: (1) a text editor, (2) a python console, and (3) a variable explorer. You can type code in the editor and run that code in the console by pressing the run button or pressing F5. When code is run in the console the variable explorer will update and show all the defined variables and their values. You can also run selected lines from the code in the editor by selecting the relevant code and pressing F9.

### Debugging

Debugging is the act of fixing broken code. Code can break in a wide variety of ways. Some errors are obvious and easy to fix, others are hidden and can take a non-trivial amount of time and resources to find and remove.

#### Simple errors

Errors due to faulty usage of the programming language are fairly easy to fix as the Python interpreter often times can point you to the exact location of a bug. Consider for instance the following code.

```python
cities = ['Amsterdam, 'London', 'Stockholm']
print(cities[3])
```

Can you spot the bug(s)? No? Let us run it and see what happens. Executing `python cities.py` (more on this later) we get the following output:

    File "cities.py", line 1
    cities = ['Amsterdam, 'London', 'Stockholm']
                        ^
    SyntaxError: invalid syntax

A `SyntaxError` means we have wrongly written a built-in Python statement. In this case we have forgotten the second `'` of the first string in the list i.e. we have made a mistake while declaring a string.

As you can see, Python informs us that the error is located on `line 1` of the script near the location of the `^` symbol. To fix this bug we have to add `'` after `Amsterdam` to form a valid string declaration. Running the script again after fixing the bug we get the following output

    File "cities.py", line 2, in <module>
    print cities[3]
    IndexError: list index out of range

An `IndexError` tells us that we are trying to access a non-existent list element. In this case we are trying to access the 4th element of a list that has 3 elements. Python again tells us where the error occurred (line 2) and shows us the actual line (`print cities[3]`). To fix this bug we need to access an existing element, say, the first element i.e, we replace `3` on line 2 with a `0` like so

```python
cities = ['Amsterdam', 'London', 'Stockholm']
print(cities[0])
```

This script executes without errors and outputs `Amsterdam`.

#### More complex errors

Unfortunately not all errors can be caught by the Python interpreter. Most errors you will encounter will be _logical_ errors i.e. the code works but is not doing what it should be doing or what you expect it to do. Take for instance the following script.

```python
cities = ['Amsterdam', 'London', 'Stockholm']

for city in range(len(cities)):
    if city == 3:
        print("You're in Scandinavia!")
```

The [len()](https://docs.python.org/3/library/functions.html#len) function returns the length of lists and strings (amongst other things). In this case it returns `3` as `cities` has three elements. The [range()](https://docs.python.org/3/library/functions.html#func-range) function takes (in its most basic form) an integer and returns a series of integers starting at `0` and ending at (but not including) the supplied integer (in this case `3`) as a list. In our case it returns `[0, 1, 2]`.

Will this script execute without errors i.e. can you spot any syntax errors? Will this script print `You're in Scandinavia!`?

Not sure? Let us try it out.

- Type over the code into the text editor in Spyder
- Save it as `cities.py`.
- execute the code by pressing the run button or F5

And? The scripts executes but there is no output, right? Why is that? In this case it is fairly easy to spot the error (`city` is never equal to 3) but often times it will not be this easy and you will need to exert more effort to find the bug. To do so effectively you will need a debugging strategy.

#### Debugging strategies

Debugging the second class of problems is what you will probably spend most of your time doing. The Python interpreter cannot help you fixing _logical_ bugs because it cannot know what your could should be doing i.e. it cannot know what you expect the code to do and whether that is different from what it is actually doing. If computers could do that we would not need to program them!

To hunt for these types of errors you can generally employ two strategies:

- use `print` statements to check the values of variables and result of functions in order to spot inconsistencies, errors or unexpected results of operations
- use a debugger: a piece of Python functionality that pauses the execution of a script and lets you execute each line separately

##### print statements

`print` statements are quick and easy to apply. They are, however, cumbersome when e.g. debugging loops (as you'll often times flood your console with output) and not always up to the task when e.g. executing scripts that take a while to finish and/or you're working with complex data structures (such as polygons).

To debug the script above all we need to do is

- add a `print` statement in the loop
- observe the output and try to determine the cause of the error

Adding a `print` statement results in the following:

```python
cities = ['Amsterdam', 'London', 'Stockholm']

for city in range(len(cities)):
    print(city)
    if city == 3:
        print("You're in Scandinavia!")
```

The output of which is

    0
    1
    2

In other words, `city` is never equal to `3` hence `if city == 3` is always `False` and the `print ("You're in Scandinavia")` is not executed. The fix: replace `3` with a `2`.

A nastier variation of this error is the following: suppose that you want to go over a list in reverse order, do something with each item and remove it from the list. You might try something like the following

```python
cities = ['Amsterdam', 'London', 'Stockholm']

for city in cities:
    print(cities.pop())
```

where `pop()` grabs the last element of the list, returns it and removes it from the list. The output is

    Stockholm
    London

Wait, where did `Amsterdam` go? Can you spot the cause of this result?

The "culprit" is the call to [pop()](https://docs.python.org/3/tutorial/datastructures.html#more-on-lists) as it removes items from the list it is called on instead of returning a modified copy and leaving the original untouched.

By calling `pop()` in the loop we are modifying (the length of) the iterable we are looping over. The result: dropped items. Why? After popping `Stockholm` on the first iteration `London` becomes the last item in the list in the second iteration. The loop terminates after it prints it and never enters the third iteration needed to access `Amsterdam`.

These are fairly straightforward examples of logical errors but hopefully it helps you understand the general problem posed by them and the corresponding debug workflow.

##### Python debugging using Spyder

To use the debugger in Spyder you can set breakpoints using the graphical interface of Spyder: click just to the right of a line number of a line of code and a red dot will appear. This dot marks the line where the debugger will pause. Press the `Debug file` button (or ctrl + F5) to start the debugger.

Open the code in Spyder, set a breakpoint and run the debugger. You will notice that, instead of the usual output in the console, you will see the following

![](img/breakpoint.jpg)
```
    ipdb> > c:\cities.py(4)<module>()
      2 
      3 for city in range(len(cities)):
1---> 4     if city == 3:
      5         print("You're in Scandinavia!")
      6 

    ipdb>
```
You have successfully paused the script (first line) on the `if city == 3:` line (fourth line) and are able to interact with it by keying

- variable and function names
- built-in commands
- debugger commands

Type `city`. What do you get? 0, right? Why? Because the script is paused in the first iteration of the `for` loop in which `city` is equal to `0`. Typing `cities` will display the list of cities. Useful, right?

The debugger can be controlled by one letter commands

- `n` - executes the current line and advances the code to the next line of the script
- `c` - unpauses the execution until it reaches another break point or the end of the script
- `q` - quits the debugger

Type `c`, press `Enter` and query the `city` variable by typing `city` followed by `Enter`. The debugger should return `1`. Why? After you pressed `c` the debugger ran the code until the following breakpoint which it encountered in the next iteration of the `for` loop. Pressing `c` once more will get you in the last iteration and pressing it once more will get you back to Bash i.e. the script finished executing.

The Spyder graphical interface makes controlling the debugging process and checking the state of variables even easier. You can step over each line by clicking the `Run current line` button (or ctrl + F10) or continue till the next breakpoint by clicking the `Continue` button (or ctrl + F12). The current line will be highlighted in the text editor. To stop de debugger you can press the `Stop Debugging` button (or ctrl + shift + F12).

The variable explorer will now show the variable values as they are at the current stage of the code. Run the debugger again and press `Run current line` twice and observe how the value of `city` changes from `0` to `1`. Convenient!

## Exercise

Execute each script in the `recap/` folder and study what the code does as a:

1.  refresher from the Resources
2.  way to discover new functions and features
