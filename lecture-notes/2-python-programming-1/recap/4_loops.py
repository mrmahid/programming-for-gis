# Loops

# loops iterate over sequences and expose their entities
# for manipulation

# we already saw how to loop through strings
# in the first recap script
country = 'The Netherlands'

print('Iterating over {}'.format(country))
for character in country:
    print(character.upper())

#
cities = ["Rotterdam", "The Hague", "Amsterdam", "Utrecht"]
print()
print('Iterating over {}. Are all elements printed on the next line?'.format(cities))
for city in cities:
    if city == "The Hague":
        # the continue statement immediately jumps to the next loop
        # iteration thereby ignoring all the code that comes
        # after it. Hint: inspect the output of the print
        # statement on line 30
        continue

    if city == "Amsterdam":
        # the break statement exits the loop altogether
        break

    print(city)


# use enumerate() to access the index and value of a sequence
# you are looping over
print()
for index, city in enumerate(cities):
    print(index, city)
    print(len(city))


# the keys/values of dictionaries are sequences and thus iterable
# in a for loop as well
populations = { "Amsterdam": 80000, "Rotterdam": 500000, "The Hague": 100000}

# we can use a loop to calculate the total amount of residents in
# Amsterdam, Rotterdam and The Hague
total_population = 0
for key in populations:
    total_population += populations[key]

# a cleaner approach is to iterate over the dict's values
total_population = 0
for value in populations.values():
    total_population += value

print(total_population)

print()
print('Iterating over {} while pop()-ing elements'.format(cities))
# WARNING: never alter a sequence while iterating over it
# Bad things will happen
for city in cities:
    print(cities.pop())


print('''
---
--- Alternatives to loops ---
---
''')
# Loops are useful artifacts but sometimes more elegant,
# shorter and or faster solutions exist
print(country.upper())

# calculate the total amount of residents in Amsterdam,
# Rotterdam and The Hague
print(sum(populations.values()))

