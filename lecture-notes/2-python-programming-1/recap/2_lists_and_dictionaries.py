# Lists and dictionaries

print('''
---
--- Lists ---
---''')

cities = ["Rotterdam", "The Hague", "Amsterdam", "Utrecht"]

print(cities)
print(cities[1])

city = 'Zwolle'

print('Adding {} to {}'.format(city, cities))
cities.append(city)
print(cities)
print()

print('Removing first element of {}'.format(cities))
cities.pop(0)
print(cities)
print()

print('Removing last element of {}'.format(cities))
cities.pop()
print(cities)
print()

print('Removing Amsterdam from list')
cities.remove('Amsterdam')
print(cities)
print()

# you can store anything you wish in a list: strings,
# other lists, lists inside lists, integers, etc.

chaos = ["Rotterdam", cities, [1, 2, 3, 4, [1, 2, 33]], "Amersfoort", 5]
print(chaos[1])

print('''
---
--- Dictionaries ---
---
''')

# dictionaries are collections of key/value pairs. The key is unique and identifies the value

populations = {
    'Amsterdam': 80000,
    'Rotterdam': 50000,
    'The Hague': 100000
}

print(populations['Amsterdam'])
print()

# use the update method to add key/value pairs to a dictionary

print('Updating {}'.format(populations))
populations.update({
    'Utrecht': 200000,
    'Groningen': 300000
})

print(populations)
print()

print("Deleting Utrecht's population")
populations.pop('Utrecht')
print(populations)
print()

print(populations.keys())
print(populations.values())
print()

# As with strings you can use the built-in dir() function
# to list all methods supported by dictionaries. Notice the update, keys and values methods in the returned list
print(dir(populations))

# dir() is an extremely useful mechanism for quickly getting a sense of an entity's capabilities