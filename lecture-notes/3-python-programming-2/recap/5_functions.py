# Functions

# functions are named and parametrized bundles
# of code that you can execute as often and
# from wherever needed. Functions take a number
# of inputs, perform operations on them and
# return the result. Functiona are executed by
# calling their name and supplying the required
# inputs

def hello_world(name):
    return 'Hello, World! My name is {}'.format(name)

print(hello_world('Sally'))

# Functions should be as short and focused as possible. They
# should, in other words, do one thing only and do it well.
# They should have descriptive names and take as few
# parameters as possible

# Use case: imagine that we would like to find the positions
# of a certain letter in a given word. In other words,
# what are the indices of all c's in 'bicycle'.


def occurrence_indices_of(letter, word):
    indices = []
    for idx, val in enumerate(word):
        if val == letter:
            indices.append(idx)

    return indices

word = 'bicycle'
letter = 'c'

print(word, occurrence_indices_of(letter, word))
print()

# Functions can be called inside loops as well
words = ['binoculars', 'circular', 'cyclone', 'circumference']
for word in words:
    print(word, occurrence_indices_of(letter, word))


# Splitting functionalities in small functions that do one
# thing well allows you to more easily combine and build
# upon them. Suppose we would like to have fuller
# printout of the occurrences statistics.

def full_occurrence_report(letter, word):
    indices = occurrence_indices_of(letter, word)

    return {
        'word': word,
        'letter': letter,
        'number_of_occurrences': len(indices),
        'indices': indices
    }

for word in words:
    print(full_occurrence_report(letter, word))


# Bonus: using list comprehensions we can reduce occurrence_indices_of() to
# a single line as

def lc_occurrence_indices_of(letter, word):
    return [idx for idx, ltr in enumerate(word) if ltr == letter]

print(lc_occurrence_indices_of('c', 'bicycle'))