# Reading and writing files
# TIP: See Chapter 4 for a longer discussion on
# reading/writing files

print('''
---
--- Reading text files ---
---

''')

# reading and writing files on disk is done
# through Python's built-in open() function

# open an existing file
data_file = open('./data/data.csv', 'r')

# read a single line
print(data_file.readline())

# data_file is a collection we
# can iterate over in a loop
temperatures = []
for line in data_file:
    cleaned_line = line.rstrip('\n')
    measurements = cleaned_line.split(',')
    temperatures.append(int(measurements[0]))

print(temperatures)
print('Max temperature is:', max(temperatures))

# close the file when done reading
data_file.close()

print('''
---
--- Writing text files ---
---
''')

# open a text file for writing
output_file = open('./data/processed.txt', 'w')

# write a single line
output_file.write('This is line #1\n')

# write many lines
for idx in range(2, 10):
    output_file.write('This is line #{}\n'.format(idx))

output_file.close()
