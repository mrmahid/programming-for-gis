# TAA3 – Selecting Python packages (Adv)
Deadline Mar 17

## Learning goals
-	Quickly and effectively find and evaluate external Python modules for their fitness of purpose for the job at hand.
-	Read online documentation and write their own documentation.

## Students will learn to: 
-	search for python packages
-	select Python packages based on their pros and cons for the job at hand
-	discuss the fit for purpose of python packages with fellow students

## Prerequisite:
- Finish chapter 5 – Documentation, packages and StackOverflow

## Assignment description
Python has a large built-in library, but it will never have everything you might need. You can create the all the missing functionality from scratch, but this can be very labor intensive. Python has a large collection of third party packages. Because of this, it is likely there might be a free python package you could use to make your job much easier.

For this assignment you will search for (at least 3) geospatial Python packages. These packages must be useful to anyone who needs to read-, write- and analyze geospatial data with Python. Make notes of the pros and cons for these packages. Use the theory from chapter 3 to evaluate the packages.

During a zoom-meeting you will compare and discuss your selected packages with a group of fellow students. As a group will make a shortlist of packages (from most usefull to less usefull), and publish it on git. At the end of the zoom meeting your group is asked to discuss the pros and cons of a selected package. 

# Deliverables:
-	Your personal list of (at least) 3 Python packages you would suggest to a anyone who needs to read-, write- and analyses geospatial data with Python.
-	A link to the published shortlist of your group.
