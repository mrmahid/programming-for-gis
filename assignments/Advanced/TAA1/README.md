# TAA1 - Starting with Python (Adv)
Deadline Feb 11

## Learning goals
- Install Python and use several functionalities of an Integrated development environment.
- Use the basic functionality of public/private collaboration platforms such as GitLab/GitHub, StackOverflow, GeoForum and others.

## Students will learn to: 
-	fork a repository on git, apply changes to files and create a merge request.
-	setup a Python environment.
-	use basic datatypes and functionality of the Python programming language.
-	read and debug code.

## Prerequisite:
- Read the course manual.
- Finish chapter 1: Working with git
- Finish chapter 2: Python programming 1 – The basics

## Assignment description
During this course git will be the main location we will use to save code and communicate with each other. In the first chapter of the lecture notes you will learn how git works and how to use its functionality. After this chapter you will know how to use git functionality like: fork, branch, pull, commit, push and merge.

In this assignment you will also setup your environment for the rest of the course. After this initial setup, the programming begins. Get acquainted with the basics of the Python programming language and utilize its different built-in datatypes and functionality.

Within the lecture notes of chapter 2 on git you will find Python resources and a step by step instruction on how to read-, use and debug code. When you get stuck during the lecture notes and/or the assignments, use the issue tracker in the main course repository.

For this formative assignment you must submit the link for your merge request on git and provide proof of your work in a Python debugger within chapter 2.

# Deliverables:
-	A link to your merge request on git.
-	A print screen of your work in a Python debugger during chapter 2.
