# TAA2 – Data ingest and cleaning (Std)
Deadline Mar 3

## Learning goals
-	Perform basic data cleaning with the Python programming language.
-	Design and setup a repeatable workflow with Python.
-	Use built-in and third-party modules/packages and create their own functions within Python.


## Students will learn to: 
-	Utilize Python functions and scripts to prepare data for an analysis.
-	Create a (semi-)automatic workflow with Python.

## Prerequisite:
-	Finish chapter 3: Python programming 2 – scripts and functions
-	Finish chapter 4: Preparing data with Python


## Assignment description
For this assignment you will assume the role of a municipal/provincial/national GIS expert tasked with processing the results of an election.

### Election results in The Netherlands
In the Netherlands there are four national elections citizens can vote in:

1.	General elections (Tweede Kamerverkiezingen)
2.	Provincial elections (Provinciale statenverkiezingen) (indirectly elects the senate)
3.	Municipal elections (Gemeenteraadsverkiezingen)
4.	Water boards elections (Waterschapsverkiezingen)

Each of these is held every four years. Only the provincial and water boards elections are held on the same day. The General elections were held, on 17 march 2021 (geojson data of the latest election isn't published yet). In this assignment we are going to take a look at this election. The complete results are published by the provinces as counts per polling station. The counts of all polling stations in a province are added together and percentages are calculated. The House of Representatives seats are subsequently assigned proportionally.

Other than the final percentages it can of course also be very interesting to see the spatial distribution of the voting results. Which areas voted left? Which areas right? Which economically liberal? Which social? Which progressive? Which conservative? Where do the nationalist parties get the most votes? Where do the environmentalist parties get their votes from? Is there a big difference in urban and rural voters? What was the approximate turnout? Etcetera.. (If you are not familiar with the political parties of the Netherlands check [this wiki page](https://en.wikipedia.org/wiki/List_of_political_parties_in_the_Netherlands)).

To make things easier for international students; here a brief generalized overview of the bigger parties:
|     Party    |     Description                        |     General Alignment    |     Economical    |     Social          |
|--------------|----------------------------------------|--------------------------|-------------------|---------------------|
|     VVD      |     Conservative-liberal party.        |     Right                |     Liberal       |     Conservative    |
|     PvdA     |     Labour Party. Social Democrats.    |     Center-Left          |     Social        |     Progressive     |
|     PVV      |     National populist party.           |     Right                |     Liberal       |     Conservative    |
|     CDA      |     Christian democratic.              |     Center-right         |     Liberal       |     Conservative    |
|     SP       |     Socialist party.                   |     Left                 |     Social        |     Progressive     |
|     GL       |     Greens.                            |     Left                 |     Social        |     Progressive     |
|     CU       |     Christian democratic.              |     Center               |     Social        |     Conservative    |
|     D66      |     Progressive-liberal party.         |     Center               |     Liberal       |     Progressive     |
|     PvdD     |     Big focus on environment.          |     Left                 |     Social        |     Progressive     |
|     SGP      |     Christian conservative.            |     Right                |     Liberal       |     Conservative    |
|     FvD      |     National populist party.           |     Right                |     Liberal       |     Conservative    |

### The assignment – Gather and transform data
You are asked to prepare election result data so it can later be used in a spatial analysis (which you will do in TAA4). The data must be cleaned and stored locally so during the analysis the percentages of votes for each party, on different administrative levels can be calculated.

To this end you are asked to design and implement an (semi-)automated data preparation pipeline that cleans and stores data in for a geospatial analysis.

The expected deliverables are 1) a Python pipeline that cleans and stores data for the analysis on the election results for different administrative levels and 2) a short essay documenting a) the pipeline, b) your design and implementation choices and c) the advantages/disadvantage (strengths and weaknesses) of your analysis pipeline. See the Deliverables and submission section for the full list of deliverables.

Your grade is based on the thoroughness and depth of the performed data preparation and the documentation of your approach and critical reflection as captured in the essay. See the Grading section for a full list of the grading criteria.

### Data
To perform the required analysis you will need data on:
-	The smaller Dutch administrative units. The Dutch Bureau of Statistics (CBS) maintains the `CBS Wijk- en buurtkaart` which contains the geometries of Dutch municipalities, districts, and neighbourhoods.
-	The Dutch provinces. There are multiple ways to get this data. CBS also maintains an overview of which municipality belongs to which province, `Gemeenten alfabetisch`. This can be used with the municipality geometries to create geometries of the provinces. Alternatively, you could search a data source with the province geometries (e.g. `Bestuurlijke Grenzen` (administrative Boundaries)).
-	The election results. These can be found as a GeoJSON at: [https://data.openstate.eu/dataset/verkiezingsuitslagen-tweede-kamerverkiezingen-2021](https://data.openstate.eu/dataset/verkiezingsuitslagen-tweede-kamerverkiezingen-2021)

Find and download the datasets required for the spatial analysis you will perform in TAA4. Explore these datasets (for example in Excel/QGIS/ArcGIS) to get a sense of their contents and what manipulations/transformations you will need to perform to get it ready for the analysis.
Feel free to add any other datasets that you find usefull or contain the needed data in a better format/structure. A good source of geospatial data is the Dutch open GIS data platform, PDOK: [https://pdok.nl/](https://pdok.nl/).


## Deliverables:
-	A zip-file containing:
    - a (series of) Python script(s) that obtain, transform and clean data in a(n) (semi-)automated manner (see the Grading section for the exact requirements)
    - 	a short essay documenting
        - 	(briefly) the data processing pipeline i.e. where does what come from, how is it transformed and how does the datacleaning work on a very general level (since the detailed implementation can be found in your code)
        -	your code design and implementation choices i.e. why did you design the data cleaning as you did.
        -   strengths and weaknesses of your pipeline.

The essay and source code should be written such that a colleague, researcher, civic hacker, etc. can easily download, execute and improve/expand it without having to contact you to ask how to run the code, which dependencies are needed, which pitfalls to look out for, etc.

## Grading criteria
You work will be graded on the following general criteria in addition to the deliverables stipulated above
-	degree and sensibility of automation - ideally the whole analysis process is automated. However, complete automation is not always feasible or wise. You should strive for a sensible balance between automation and manual work, but whatever you choose: document it thoroughly.
-	use of tools and modules - when you need to Get Things Done™ it is wise to use existing tooling as much as possible (instead of building your own). Use of third-party Python and modules/libraries/APIs is greatly encouraged.
-	code quality - Is your code readable and does it run efficiently.

### Grading rubric
Please refer to the grading rubric for the complete list of grading criteria.
